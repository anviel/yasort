"""
Lens design using some simple "recipes"
"""
import numpy as np
from typing import Tuple
import medium

class DesignFailureException(Exception):
   pass

def half_width(radius: float, diameter: float) -> float:
   """
   Compute the half width of spherical lens defined by its radius
   of curvature, and its diameter around the optical axis
   """
   return np.abs(radius) - np.sqrt(radius**2-0.25*diameter**2)

def simple_achromat(
             f: float,
             m1: medium.Medium,
             m2: medium.Medium,
             wl1: float,
             wl2: float,
             wlc: float,
             diam: float) -> Tuple[float,float]:
   """
   Design a simple achromat doublet lens (Fraunhofer type) made of:
   - a biconvex lens in crown glass
   - a planoconcave in flint glass

   f is the desired effective focal length
   m1 is the medium of the first lens (crown)
   m2 is the medium of the second lens (flint)
   wl1 is the first corrected wavelength, in nanometer
   wl2 is the second corrected wavelength, in nanometer
   wlc is the wavelength that defines the focal length, in nanometer
   diam is the diameter of the lens, it is used for checking that
        the radii are not smaller than the semi-diameter

   It returns a tuple of three values:
   - the radius of first surface, > 0
   - the radius of second (internal) surface, usually < 0
   """
   n1, n2 = m1.get_index(wlc), m2.get_index(wlc)
   dn1 = m1.get_index(wl1)-m1.get_index(wl2)
   dn2 = m2.get_index(wl1)-m2.get_index(wl2)
   a = np.array(( (n1-1, n2-n1),
                  (dn1,  dn2-dn1) ), dtype=np.float64)
   b = np.array((1/f, 0.0), dtype=np.float64)
   c = np.linalg.solve(a, b)
   if c[0] > 0:
      r1 = 1/c[0]
   else:
      print("1/r1 = {:12.5e}".format(c[0]))
      raise DesignFailureException
   if c[1] != 0:
      r2 = 1/c[1]
   else:
      print("1/r2 = {:12.5e}".format(c[1]))
      raise DesignFailureException
   if r1 < diam/2:
      print("r1 = {:12.5e} < diam/2 = {:12.5e}".format(r1, 0.5*diam))
      raise DesignFailureException
   if np.abs(r2) < diam/2:
      print("|r2| = {:12.5e} < diam/2 = {:12.5e}".format(np.abs(r2), 0.5*diam))
      raise DesignFailureException
   return r1, r2
