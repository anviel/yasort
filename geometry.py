"""
Geometry routines
"""
import numpy as np
from numpy.linalg import norm
from numpy.testing import assert_allclose


def intersect_line_circle(R: float,
                          X: np.ndarray,
                          U: np.ndarray) -> np.ndarray:
   """
   Intersect of a circle of radius R, centered at origin
   with a line going through X in direction U.
   U is assumed to be a unit vector
   """
   assert_allclose(norm(U), 1)
   # the intersects are given by the roots of a quadratic equation
   t = np.roots(np.poly1d([1.0, 2*np.dot(X, U), norm(X)**2-R**2]))
   # look for real roots
   t = np.array([ti for ti in t if np.isclose(np.abs(np.imag(ti)), 0)],
                dtype=np.float64)
   return np.sort(t)

def intersect_ray_circle(R: float,
                         X: np.ndarray,
                         U: np.ndarray) -> np.ndarray:
   """
   Intersect of a circle of radius R, centered at origin
   with a ray (an half-line) of origin X in direction U.
   U is assumed to be a unit vector
   """
   assert_allclose(norm(U), 1)
   # full circle
   t = intersect_line_circle(R, X, U)
   # filter only the roots greater than 0
   t = np.array([ti for ti in t if ti >= 0], dtype=np.float64)
   # no roots
   if t.size == 0:
      return t
   # get the smallest
   return np.min(t)

def intersect_ray_arc(R: float,
                      X: np.ndarray,
                      U: np.ndarray,
                      d: float,
                      east: bool = True) -> np.ndarray:
   """
   Compute the intersect of a ray with an arc.
   The arc is a part of a circle of radius R, with center at origin (0,0).
   If the arc is at the east of the circle it goes through the point (1,0).
   it is contained in the box [sqrt(R^2-d^2/4), R] x [-d/2, +d/2].
   If east is False, the arc is at the west of the circle and it goes 
   through the point (-1,0). It is contained in the box
   [-R, -sqrt(R^2-d^2/4)] x [-d/2, +d/2].
   R is the radius of the circle
   X is the origin point of the ray
   U is the unit vector that defines the direction of the ray
   d is the diameter of the arc
   east is a boolean value.
   """
   assert_allclose(norm(U), 1)
   # define the bounding box
   xmin, xmax, ymin, ymax = np.sqrt(R**2 - 0.25*d**2), R, -0.5*d, 0.5*d
   if not east:
      xmin, xmax = -xmax, -xmin
   def in_bb(t):
      return t >= 0 \
             and xmin <= X[0]+t*U[0] <= xmax \
             and ymin <= X[1]+t*U[1] <= ymax
   # full circle
   t = intersect_line_circle(R, X, U)
   # filter the points that are in the bounding box
   t = np.array([ti for ti in t if in_bb(ti)], dtype=np.float64)
   if t.size == 0:
      return t
   else:
      # many roots, get the min
      t = np.min(t)
   # return the point
   return X + t * U

def intersect_ray_line(X: np.ndarray, U: np.ndarray) -> np.ndarray:
   """
   Intersect a ray with the horizontal line y=0 
   """
   assert_allclose(norm(U), 1)
   if np.isclose(U[1], 0):
      return np.empty((0,))
   t = -X[1] / U[1]
   if t < 0.0:
      return np.empty((0,))
   return X + t * U

def intersect_ray_segment(X: np.ndarray, U: np.ndarray, d: float) -> np.ndarray:
   """
   Intersect a ray with the segment of the horizontal line defined by
   -d/2 <= x <= +d/2 and y = 0 
   """
   pt = intersect_ray_line(X, U)
   if pt.size == 0 or pt[0] < -0.5*d or pt[0] > 0.5*d:
      return np.empty((0,))
   return pt

def intersect_line_parabola(X: np.ndarray,
                            U: np.ndarray,
                            f: float) -> np.ndarray:
   """
   Intersect of a line (defined by point X and direction U)
   and a parabola of equation x^2 = 4.f.z
   f is the focal length of the parabola. It can be positive or negative.
   U is assumed to be a unit vector
   """
   assert_allclose(norm(U), 1)
   # the point X+t*U must satisfy the parabola equation
   t = np.roots(np.poly1d([U[0]**2,
                           2.0*U[0]*X[0] - 4.0*f*U[1],
                           X[0]**2 - 4.0*f*X[1]]
                         ))
   # look for real roots
   t = np.array([ti for ti in t if np.isclose(np.abs(np.imag(ti)), 0)],
                dtype=np.float64)
   return np.sort(t)

def intersect_ray_parabola(X: np.ndarray,
                           U: np.ndarray,
                           f: float,
                           d: float) -> np.ndarray:
   """
   Intersect a ray with an arc of parabola, limited by diameter d.
   """
   # intersect the line with the full parabola
   t = intersect_line_parabola(X, U, f)
   # only the positive roots with x in [-d/2, +d/2]
   t = np.array([ti for ti in t if ti >= 0 and -0.5*d <= X[0]+ti*U[0] <= 0.5*d],
                dtype=np.float64)
   if t.size == 0:
      return t  # an empty array
   return X + np.min(t) * U

def cross_product(a: np.ndarray, b: np.ndarray) -> np.ndarray:
   """
   Compute the cross product of two vectors.
   The result is always a 3D vector.
   """
   assert a.size == b.size
   if a.size == 2:
      # the result is a 3D vector with only the last element not zero
      return np.array((0., 0., a[0]*b[1]-a[1]*b[0]), dtype=np.float64)
   else:
      # redirect to numpy function for 3D vectors
      return np.cross(a, b)

def reflection(r: np.ndarray, v: np.ndarray) -> np.ndarray:
   """
   Reflect a ray r at a surface defined by its normal v.
   v is assumed to be a unit vector.
   """
   assert_allclose(norm(v), 1)
   return r - 2*np.dot(v, r)*v

def refraction(r: np.ndarray,
               v: np.ndarray,
               n1: float, n2: float) -> np.ndarray :
   """
   Refract a ray v at a surface defined by its normal v.
   r is a unit vector incident to the surface
   v is the unit vector along the normal to the surface
   n1 and n2 are the refractive indices. n1 is the index
   of the medium of the incident ray, and n2 is the index
   of the medium of the refracted ray.
   """
   assert_allclose(norm(r), 1)
   assert_allclose(norm(v), 1)
   # incident angle
   cosi1 = np.dot(r, v)
   sini1 = cross_product(r, v)[2]
   # Snell's law
   sini2 = n1*sini1/n2
   if  sini2 > 1:
      # total internal reflection
      return reflection(r, v)
   # cos(i2) and cos(i1) must have same sign
   cosi2 = np.sqrt(1.0 - sini2**2) * np.sign(cosi1)
   # Mouroulis' formula, p. 318
   r2 = (n1*r + (n2*cosi2 - n1*cosi1)*v)/n2
   return r2 / norm(r2)

def orthogonal(x: np.ndarray) -> np.ndarray:
   """
   Return a new vector orthogonal to a given vector x,
   obtained by a counter clockwise rotation of 90 degrees.
   """
   return np.array((-x[1], x[0]), dtype=np.float64)

def diffraction_grating(ray       : np.ndarray,
                        n_m_lambda: float,
                        reflection: bool) -> np.ndarray:
   """
   Diffraction of an incident ray r over a grating,
   characterized by the product n_m_lambda = order * density * wavelength,
   the normal of the grating being vertical.
   Density and wavelength must have consistent units, for example
   density in lines/mm and wavelength in mm.
   If reflection is True, it is a diffraction grating,
   if False, it is a transmission grating.
   The ray is assumed to be a unit vector.
   """
   assert_allclose(norm(ray), 1)
   # the sign that appears in the grating equation according to its type
   if reflection:
      gsign = 1
   else:
      gsign = -1  # transmission
   # local frame vectors
   Xl = np.array((1., 0.), dtype=np.float64)  # along the grating surface
   Yl = np.array((0., 1.), dtype=np.float64)  # grating normal
   # check that the ray is incident
   if np.dot(ray, Yl) > 0:
      return np.empty((0,))
   # sine of incident angle, with Schroeder's sign convention
   sinalpha = np.dot(ray, Xl)
   # sine of diffracted angle
   sinbeta = n_m_lambda - gsign*sinalpha
   # check that there is diffraction
   if np.abs(sinbeta) > 1:
      return np.empty((0,))
   cosbeta = np.sqrt(1.0 - sinbeta**2)
   return gsign*np.array((-sinbeta, cosbeta), dtype=np.float64)

def polar(r: float, theta: float) -> np.ndarray:
   """
   Create a 2D vector using polar coordinates
   r: the norm of the vector
   theta: the angle of the vector, in radian
   """
   return np.array((r*np.cos(theta), r*np.sin(theta)), dtype=np.float64)

