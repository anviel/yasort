"""
Define classes for optical ray tracing
"""
import numpy as np
from numpy.linalg import norm
from numpy.testing import assert_allclose
import geometry
import medium
#import pdb

def ray_color(wavelength: float) -> str:
   """
   Set color according to wavelength
   """
   if wavelength < 500:
      return "blue"
   elif wavelength <= 560:
      return "green"
   elif wavelength <= 600:
      return "yellow"
   else:
      return "red"

class Ray:
   """
   Define a ray of light as an half-line, defined by a starting
   point (origin) and a direction of propagation.
   The ray carries the refraction index of the index it is propagating
   and its wavelength;
   """
   def __init__(self,
                origin: np.ndarray,
                direction: np.ndarray,
                indx: float = 1.0,
                wavelength: float = 500.0):
      # the following four attributes are public
      self.origin = origin         # starting point
      self.direction = direction / norm(direction)  # unit vector
      self.indx = indx             # refraction index
      self.wavelength = wavelength # nanometer
      self._stop = False
   def stop_here(self):
      """
      Do not propagate any more
      """
      self._stop = True
   @property
   def stop(self) -> bool:
      return self._stop 
   def draw(self, next_ray, rpen: str = "") -> str:
      """
      Return an asymptote draw command
      """
      return "draw(({:12.5e},{:12.5e})--({:12.5e},{:12.5e}), p={});\n".format(
            self.origin[0], self.origin[1],
            next_ray.origin[0], next_ray.origin[1],
            rpen if rpen != "" else ray_color(self.wavelength))

class Surface:
   """
   A generic optical surface, defined by its vertex, and normal.
   The normal is usually at the opposite of the propagation of light.
   """
   def __init__(self,
                vertex: np.ndarray,
                normal: np.ndarray,
                diameter: float,
                medium_: medium.Medium):
      # the following three attributes are public
      self.vertex = vertex
      self.normal = normal / norm(normal)
      self.diameter = diameter
      self.medium = medium_
      # define default for the origin and axes of the local frame
      self.center = np.zeros((2,), dtype=np.float64)
      self.basis = np.eye(2, dtype=np.float64)
   def to_local_frame(self, ray: Ray) -> Ray:
      """
      Convert the ray from global coordinates to surface coordinates
      """
      return Ray(np.dot(self.basis.T, ray.origin-self.center),
                 np.dot(self.basis.T, ray.direction),
                 ray.indx,
                 ray.wavelength)
   def to_global_frame(self, ray: Ray) -> Ray:
      """
      Convert the ray from surface local coordinates to global coordinates
      """
      return Ray(self.center + np.dot(self.basis, ray.origin),
                 np.dot(self.basis, ray.direction),
                 ray.indx,
                 ray.wavelength)
   def propagate(self, ray: Ray) -> Ray:
      """To be defined in subclasses"""
      return ray  # do nothing
   def draw(self) -> str:
      """To be defined in subclasses"""
      return ""

class Sphere(Surface):
   """
   A transparent spherical surface where refracion takes place.
   If radius is positive the surface is convex w.r.t. to incident ray.
   The diameter parameter defines the part of the circle around the
   optical axis that defines the surface.
   """
   def __init__(self,
                vertex: np.ndarray,
                normal: np.ndarray,
                radius: float,
                diameter : float,
                medium_: medium.Medium):
      super().__init__(vertex, normal, diameter, medium_)
      # define the origin and axes of the local frame
      self.center = self.vertex - radius * self.normal
      self.basis = np.vstack((-normal, -geometry.orthogonal(normal))).T
      self.positive_curvature = radius > 0
      self.radius = np.abs(radius)
   def get_local_normal(self, p: np.ndarray) -> np.ndarray:
      """
      The unit vector normal to the surface at point p
      """
      assert_allclose(norm(p), self.radius) 
      assert -self.diameter/2.0 <= p[1] <= self.diameter/2.0
      v = p / norm(p)
      return v if self.positive_curvature else -v
   def propagate(self, ray: Ray) -> Ray:
      """
      Compute the refraction of the ray through the surface
      """
      if ray.stop:
         return ray
      local_ray = self.to_local_frame(ray)
      point = geometry.intersect_ray_arc(
                  self.radius,
                  local_ray.origin,
                  local_ray.direction,
                  self.diameter,
                  not self.positive_curvature)
      if point.size == 0:
         ray.stop_here()
         return ray
      local_index = self.medium.get_index(local_ray.wavelength)
      refracted_dir = geometry.refraction(
                        local_ray.direction,
                        self.get_local_normal(point),
                        local_ray.indx,
                        local_index)
      return self.to_global_frame(
               Ray(point,
                   refracted_dir,
                   local_index,
                   local_ray.wavelength))
   def draw(self) -> str:
      """
      Draw the surface as an arc of circle
      """
      # asymptote arc() needs angle in degrees, CCW from angle1 to angle2
      half_angle = 180.0/np.pi*np.arcsin(0.5*self.diameter/self.radius)
      axis_angle = 180.0/np.pi*np.arctan2(self.normal[1], self.normal[0])
      if not self.positive_curvature:
         axis_angle -= 180.0
      angle1, angle2 = axis_angle - half_angle, axis_angle + half_angle
      return "// spherical surface ({}), radius = {}, diameter = {}\n" \
             "draw(arc(({:12.5e},{:12.5e}), {:12.5e}, {:12.5e}, {:12.5e}), "\
             "p=psurf);\n".format(
               self.__class__.__name__,
               self.radius*(1 if self.positive_curvature else -1),
               self.diameter,
               self.center[0], self.center[1],
               self.radius, angle1, angle2)

class SphereMirror(Sphere):
   """A reflective spherical surface"""
   def __init__(self,
                vertex: np.ndarray,
                normal: np.ndarray,
                radius: float,
                diameter: float):
      super().__init__(vertex, normal, radius, diameter, medium.Medium())
   def propagate(self, ray: Ray) -> Ray:
      """
      Compute the reflection of a ray on the surface
      """
      if ray.stop:
         return ray
      local_ray = self.to_local_frame(ray)
      point = geometry.intersect_ray_arc(
                  self.radius,
                  local_ray.origin,
                  local_ray.direction,
                  self.diameter,
                  not self.positive_curvature)
      if point.size == 0:
         ray.stop_here()
         return ray
      reflected_dir = geometry.reflection(
                        local_ray.direction,
                        self.get_local_normal(point))
      return self.to_global_frame(
                  Ray(point,
                      reflected_dir,
                      local_ray.indx,
                      local_ray.wavelength))

class Plane(Surface):
   """
   A generic class for a plane surface
   """
   def __init__(self,
                vertex: np.ndarray,
                normal: np.ndarray,
                diameter: float,
                medium_: medium.Medium):
      super().__init__(vertex, normal, diameter, medium_)
      # define the origin and axes of the local frame
      self.center = np.copy(vertex)
      self.basis = np.vstack((-geometry.orthogonal(normal), normal)).T
   def draw(self, pen: str = "pplane") -> str:
      """Draw the plane as a segment"""
      end1 = self.center \
             + np.dot(self.basis,
                      np.array((-0.5*self.diameter, 0), dtype=np.float64))
      end2 = self.center \
             + np.dot(self.basis,
                      np.array((0.5*self.diameter, 0), dtype=np.float64))
      return "// plane ({})\n"\
             "draw(({:12.5e},{:12.5e})--({:12.5e},{:12.5e}), "\
             "p={});\n".format(
               self.__class__.__name__,
               end1[0], end1[1], end2[0], end2[1], pen)

class Screen(Plane):
   """A plane screen that absorbs the light and stops it"""
   def __init__(self,
                vertex: np.ndarray,
                normal: np.ndarray,
                diameter: float):
      super().__init__(vertex, normal, diameter, medium.Medium())
   def propagate(self, ray: Ray) -> Ray:
      """
      Compute the intersection of a ray with the screen
      """
      if ray.stop:
         return ray
      local_ray = self.to_local_frame(ray)
      point = geometry.intersect_ray_segment(
                  local_ray.origin,
                  local_ray.direction,
                  self.diameter)
      if point.size == 0:
         ray.stop_here()
         return ray
      final_ray = self.to_global_frame(
                     Ray(point, ray.direction, ray.indx, ray.wavelength))
      final_ray.stop_here()
      return final_ray

class TransparentPlane(Plane):
   """
   A plane that let the light go through it
   """
   def __init__(self,
                vertex: np.ndarray,
                normal: np.ndarray,
                diameter: float,
                medium_: medium.Medium):
      super().__init__(vertex, normal, diameter, medium_)
   def propagate(self, ray: Ray) -> Ray:
      """
      Compute the refraction of a ray through the plane
      """
      if ray.stop:
         return ray
      local_ray = self.to_local_frame(ray)
      point = geometry.intersect_ray_line(local_ray.origin, local_ray.direction)
      if point.size == 0:
         ray.stop_here()
         return ray
      local_index = self.medium.get_index(ray.wavelength)
      refracted_dir = geometry.refraction(
                           local_ray.direction,
                           np.array((0., 1.), dtype=np.float64),
                           ray.indx,
                           local_index)
      return self.to_global_frame(
               Ray(point, refracted_dir, local_index, ray.wavelength))

class PlaneMirror(Plane):
   """A plane mirror"""
   def __init__(self,
                vertex: np.ndarray,
                normal: np.ndarray,
                diameter: float):
      super().__init__(vertex, normal, diameter, medium.Medium())
   def propagate(self, ray: Ray) -> Ray:
      """
      Compute the reflection of a ray on the plane
      """
      if ray.stop:
         return ray
      local_ray = self.to_local_frame(ray)
      point = geometry.intersect_ray_segment(
                  local_ray.origin,
                  local_ray.direction,
                  self.diameter)
      if point.size == 0:
         ray.stop_here()
         return ray
      # in the local frame, the normal to the plane is vertical
      reflected_dir = geometry.reflection(
                        local_ray.direction,
                        np.array((0., 1.), dtype=np.float64))
      return self.to_global_frame(
               Ray(point, reflected_dir, ray.indx, ray.wavelength))

class DiffractionGrating(Plane):
   """
   A diffraction grating, either of reflexion type or transmission type.
   """
   def __init__(self,
                vertex    : np.ndarray,
                normal    : np.ndarray,
                diameter  : float,
                nm        : float,
                reflection: bool):
      super().__init__(vertex, normal, diameter, medium.Medium())
      # the product order * density (in lines/mm)
      self.nm = nm
      self.reflection = reflection
   def propagate(self, ray: Ray) -> Ray:
      """
      Compute the diffraction of a ray over a grating
      """
      if ray.stop:
         return ray
      local_ray = self.to_local_frame(ray)
      point = geometry.intersect_ray_segment(
                  local_ray.origin,
                  local_ray.direction,
                  self.diameter)
      if point.size == 0:
         ray.stop_here()
         return ray
      # convert wavelength from nanometer to millimeter
      diffrac_dir = geometry.diffraction_grating(
                              local_ray.direction,
                              self.nm*local_ray.wavelength*1e-6,
                              self.reflection)
      # diffraction can be forbidden for some values of
      # order, density, wavelength and incident angle
      if diffrac_dir.size == 0:
         ray.stop_here()
         return ray
      return self.to_global_frame(
               Ray(point, diffrac_dir, local_ray.indx, local_ray.wavelength))
                           
   def draw(self):
      """
      Like a plane, just change the pen
      """
      return super().draw("pgrating")

class Paraboloid(Surface):
   """
   A paraboloidal surface, a cap of paraboloid, limited by diameter,
   and defined by its focal length
   """
   def __init__(self,
                vertex: np.ndarray,
                normal: np.ndarray,
                flength: float,
                diameter: float,
                medium_: medium.Medium):
      super().__init__(vertex, normal, diameter, medium_)
      # define the local frame
      self.center = np.copy(vertex)
      self.basis = np.vstack((-geometry.orthogonal(normal), normal)).T
      self.positive_curvature = flength > 0
      self.flength = np.abs(flength)
   def get_local_normal(self, p: np.ndarray) -> np.ndarray:
      """
      The unit vector of the outer normal to the surface at point p
      """
      # check that p satisfy the parabola equation
      assert_allclose(p[0]**2, 4.0*self.get_curvature_sign()*self.flength*p[1])
      assert -self.diameter/2.0 <= p[0] <= self.diameter/2.0
      # gradient
      v = np.array((-2.0*p[0],
                     4.0*self.get_curvature_sign()*self.flength),
                   dtype=np.float64)
      # get direction according to sign
      return self.get_curvature_sign() * v / norm(v)
   def get_curvature_sign(self):
      """
      Take the opposite of the sign of curvature
      to have the actual sign of the focal length
      """
      return -1 if self.positive_curvature else 1
   def propagate(self, ray: Ray) -> Ray:
      """
      Compute the refraction of a ray through the paraboloidal surface
      """
      if ray.stop:
         return ray
      local_ray = self.to_local_frame(ray)
      point = geometry.intersect_ray_parabola(
                  local_ray.origin,
                  local_ray.direction,
                  self.flength*self.get_curvature_sign(),
                  self.diameter)
      if point.size == 0:
         ray.stop_here()
         return ray
      local_index = self.medium.get_index(ray.wavelength)
      refracted_dir = geometry.refraction(
                        local_ray.direction,
                        self.get_local_normal(point),
                        ray.indx,
                        local_index)
      return self.to_global_frame(
               Ray(point,
                   refracted_dir,
                   local_index,
                   local_ray.wavelength))
   def draw(self) -> str:
      """
      Draw an arc of parabola, sampling the parabolic curve
      to form an asymptote path
      """
      s = ""
      dstep = self.diameter / 24.0  # number of sample points
      for x in np.arange(-0.5*self.diameter, 0.5*self.diameter+dstep, dstep):
         # parabola equation, taking the sign into account
         z = x**2/(4*self.get_curvature_sign()*self.flength)
         # to global frame
         local_p = np.array((x, z), dtype=np.float64)
         p = self.center + np.dot(self.basis, local_p)
         # format the output
         if s:
            s += "--({:12.5e},{:12.5e})".format(p[0], p[1])
         else:
            s = "// parabola ({}), focal length = {}, diameter = {}\n"\
                "draw(({:12.5e},{:12.5e})"\
                .format(self.__class__.__name__,
                        -self.get_curvature_sign()*self.flength,
                        self.diameter, p[0], p[1])
      s += ", p=psurf);\n"
      return s

class ParaboloidMirror(Paraboloid):
   def __init__(self,
                vertex: np.ndarray,
                normal: np.ndarray,
                flength: float,
                diameter: float):
      super().__init__(vertex, normal, flength, diameter, medium.Medium())
   def propagate(self, ray: Ray) -> Ray:
      """
      Compute the reflection of a ray on the paraboloidal surface
      """
      if ray.stop:
         return ray
      local_ray = self.to_local_frame(ray)
      point = geometry.intersect_ray_parabola(
                  local_ray.origin,
                  local_ray.direction,
                  self.flength*self.get_curvature_sign(),
                  self.diameter)
      if point.size == 0:
         ray.stop_here()
         return ray
      reflected_dir = geometry.reflection(
                        local_ray.direction,
                        self.get_local_normal(point))
      return self.to_global_frame(
               Ray(point, reflected_dir, ray.indx, ray.wavelength))

class FreeSpace(Surface):
   """
   This is not really a surface.
   This is just a space that let the rays propagate freely
   up to a maximum distance.
   """
   def __init__(self, max_prop_dist: float):
      super().__init__(np.zeros((2,), dtype=np.float64),
                       np.array((1., 0.), dtype=np.float64),
                       0.,
                       medium.Medium())
      # maximum distance of propagation
      self.max_prop_dist = max_prop_dist
   def propagate(self, ray: Ray) -> Ray:
      """
      Propagate freely along the direction, up to a maximum distance
      """
      if ray.stop:
         return ray
      new_ray = Ray(ray.origin + self.max_prop_dist * ray.direction,
                    ray.direction,
                    ray.indx,
                    ray.wavelength)
      new_ray.stop_here()
      return new_ray
   def draw(self) -> str:
      return ""
