"""
Optical media
"""
import numpy as np

class Medium:
   """A generic optical medium model"""
   def __init__(self):
      pass
   def get_index(self, wl: float) -> float:
      return 1.0

class ConstantMedium(Medium):
   """An optical medium with constant refraction index"""
   def __init__(self, constindex: float):
      self._constindex = constindex
   def get_index(self, wl: float) -> float:
      return self._constindex

class SellmeierMedium(Medium):
   """
   Optical medium with index defined by Sellmeier's equation.
   The form of the equation is:

   n^2 = A + B/(1-C/lambda^2) + D/(1-E/lambda^2)

   with lambda the wavelength expressed in micrometer.

   It is found in ref.
   - Ghosh G., Sellmeier coefficients and dispersion of thermo-optic
     coefficients for some optical glasses,
     in Applied Optics, Vol. 36, No. 7, Mars 1997.
   - https://refractiveindex.info
   """
   def __init__(self,
                A: float, B: float, C: float, D: float, E: float,
                F: float = 0.0, G: float = 0.0):
      self.A = A
      self.B = B
      self.C = C * 0.01
      self.D = D
      self.E = E
      self.F = F
      self.G = G
   def get_index(self, wl: float) -> float:
      """
      Return index, wl is wavelength in nanometer
      """
      assert wl > 0.0
      wu = wl * 1e-3  # convert from nanometer to micrometer
      return np.sqrt(self.A + self.B/(1.0-self.C/wu**2)
                            + self.D/(1.0-self.E/wu**2)
                            + self.F/(1.0-self.G/wu**2))
   def get_Abbe_number(self, wl: float) -> float:
      """
      Return Abbe's number, or constringence
      """
      den = self.get_index(486.1) - self.get_index(656.3)
      if den != 0.0:
         return (self.get_index(589.3) - 1.0)/den
      else:
         return 0.0

# Now define some glasses provided in Ghosh ref.
air = ConstantMedium(1.0)
BaLF4 = SellmeierMedium(1.64229267, 0.80996963, 1.84821497, 1.32867267, 150)
BaK1 = SellmeierMedium(1.53163489, 0.90159230, 1.55142286, 1.25742434, 150)
PSK3 = SellmeierMedium(1.45898859, 0.91813830, 1.30700097, 1.53653204, 150)
SK4  = SellmeierMedium(1.53418270, 1.02432620, 1.47483619, 0.97315045, 100)
SSK4A = SellmeierMedium(1.60142499, 0.96932959, 1.66620095, 1.17740089, 130)
K5 = SellmeierMedium(1.50879617, 0.77615273, 1.52773045, 1.26816265, 150)
LaF2 = SellmeierMedium(1.90798862, 1.05969452, 2.35627833, 1.65229731, 150)
BK7 = SellmeierMedium(1.43131380, 0.84014624, 1.28897582, 0.97506873, 100)
FK5 = SellmeierMedium(1.36459305, 0.82393169, 1.10838681, 0.93306279, 100)
LaKF10 = SellmeierMedium(1.77798367, 1.11987423, 1.90728779, 1.28954539, 90)
KzFS6 = SellmeierMedium(1.70910663, 0.77858837, 2.12286099, 0.88503243, 70)
ZK1 = SellmeierMedium(1.53023177, 0.78537672, 1.58905079, 1.03208380, 120)
SF6 = SellmeierMedium(2.07890466, 1.03775076, 4.23019093, 1.39213190, 150)
BaF10 = SellmeierMedium(1.0, 1.5851495, 0.926681282, 0.143559385, 0.0424489805, 1.08521269, 105.613573)
SF5 = SellmeierMedium(1.0, 1.52481889, 1.1254756, 0.187085527, 0.0588995392, 1.42729015, 129.141675)
