YASORT
======
YASORT stands for *Yet Another Sequential Optical Ray Tracer*.
This is a Python package that provides classes and functions for modeling and simulation of optical systems using geometrical optics and ray tracing method.
The package allows to define reflecting or refracting surfaces of various shapes (plane, spherical cap or paraboloid), optical media like glasses characterized by a wavelength-dependent refraction index, and provides system analysis methods, like the computation of the effective focal length.

**At this time, the package is limited to two-dimensional models of  systems with rotational symmetry.** This means that the rays are all propagating in the meridional plane containing the optical axis.

To build and simulate the model of an optical system, one has to write a small Python script that instantiates the classes provided by the modules of the package.

Dependencies
==
* Python 3
* Numpy
* Matplotlib is not required but useful for analysis of results

Getting started
==
Write a Python script to study a very simple biconvex lens.
First import the modules :
```python
    import numpy as np
    import medium
    import optics
    import lens
    import analysis
```

Define an optical medium (a glass with constant refraction index 1.5) and some parameters of the lens :
```python
    glass = medium.ConstantMedium(1.5)
    radius = 500.0
    diam = 150.0
    half_width = lens.half_width(radius, diam)
```

Build the system as a sequence of two convex surfaces (two spherical caps defined by a vertex, a unit normal vector, a radius of curvature, and a diameter) followed by a plane screen (defined by a vertex, a unit normal vector and a width) :
```python
    biconvex = [
      optics.Sphere(
         np.array((0., 0.)),
         np.array((-1., 0.)),
         radius,
         diam,
         glass),
      optics.Sphere(
         np.array((2*half_width, 0.)),
         np.array((-1., 0.)),
         -radius,
         diam,
         medium.air),
      optics.Screen(
         np.array((499, 0.0)),
         np.array((-1.0, 0.0)),
         300)
    ]
```

Compute the effective focal length for a wavelength of 500 nanometer:
```python
    efl, _ = analysis.efl(biconvex, 500)
    print("EFL of system is {} mm".format(efl))
```

Documentation
==
Documentation is available in `doc/manuel_utilisateur.pdf` (at this time, only a french version is available).

Examples
==
Examples are available in the `systems` folder. Each Python script describes an optical system and performs some basic analysis (propagation of a bundle of rays through the system, focal length computation).
