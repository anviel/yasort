"""
Optical analysis methods using ray tracing
"""
import numpy as np
from numpy.linalg import norm
import geometry
import optics
from typing import List, Tuple
import pdb


def propagate(syst: List[optics.Surface],
              ray_bundle: List[optics.Ray]) -> List[List[optics.Ray]]:
   """
   Propagate a bundle of rays through an optical system
   """
   ray_bundle_seq = [ ray_bundle ]
   for surf in syst:
      new_ray_bundle = []
      for r in ray_bundle_seq[-1]:
         new_ray_bundle.append(surf.propagate(r))
      ray_bundle_seq.append(new_ray_bundle)
   return ray_bundle_seq

def optical_axis(syst: List[optics.Surface]) -> List[optics.Ray]:
   """
   Compute the optical axis of the system
   """
   # create a ray through the optical axis of first surface
   r = optics.Ray(syst[0].vertex+0.5*syst[0].diameter*syst[0].normal,
                  -syst[0].normal,
                  1.0, 500)
   oaxis = [r]
   for surf in syst:
      oaxis.append(surf.propagate(oaxis[-1]))
   return oaxis

def draw(filename: str,
         syst: List[optics.Surface],
         ray_bundle_sequence: List[List[optics.Ray]],
         oaxis: List[optics.Ray] = []):
   """
   Draw the system with its optional optical axis,
   and the bundle of rays propagating through the system
   """
   with open(filename, "w") as f:
      f.write("// -- definitions\npen psurf = gray;\n"
              "pen pplane = gray;\n"
              "pen pgrating = dotted;\n")
      f.write("// -- surfaces\n");
      for surf in syst:
         f.write(surf.draw())
      if oaxis:
         f.write("// -- optical axis\n")
         for i in range(len(oaxis)-1):
            r, rn = oaxis[i], oaxis[i+1]
            if not r.stop:
               f.write(r.draw(rn, rpen="black+dashed"))
      f.write("// -- ray bundles\n")
      for i in range(len(ray_bundle_sequence)-1):
         f.write("// rays to surface {}\n".format(i+1));
         for r, rn in zip(ray_bundle_sequence[i], ray_bundle_sequence[i+1]):
            if not r.stop:
               f.write(r.draw(rn))

def make_ray_bundle(x: float,
                    ymin: float,
                    ymax: float,
                    nray: int,
                    theta: float,
                    wavelen: float) -> List[optics.Ray]:
   """
   Create a bundle of nray rays at abscissa x, inclined of theta
   with ordinates between ymin and ymax
   """
   if ymax < ymin:
      ymax, ymin = ymin, ymax
   assert ymax > ymin
   assert nray > 1
   ystep = (ymax - ymin) / (nray - 1)
   return [ optics.Ray( np.array((x, y)),
                        np.array((np.cos(theta), np.sin(theta))),
                        1.0,
                        wavelen)
            for y in np.arange(ymin, ymax+ystep, ystep) ]

def efl(syst: List[optics.Surface],
        wavelength: float) -> Tuple[float, np.ndarray]:
   """
   Compute the effective focal length (E.F.L.) of an optical system
   for a given wave length.
   It returns the EFL, followed by the datas used to compute it, i.e.
   the distance Y of the incident test rays w.r.t. the input optical axis,
   and the angle THETA of the output test rays w.r.t. the output optical axis.
   The EFL is obtained by fitting a straight line: Y = EFL * THETA
   """
   # get the input & output optical axis
   oaxis = optical_axis(syst)
   input_oaxis = oaxis[0]
   output_oaxis = oaxis[-1]
   # get a direction orthogonal to the input optical axis
   vortho = geometry.orthogonal(input_oaxis.direction)
   # nb of test rays
   nr = 10
   # prepare the output arrays:
   # - y is the distance to the input optical axis
   # - angle is the angle of the test rays w.r.t. to the output optical axis
   y = np.zeros((nr+1,), dtype=np.float64)
   angle = np.zeros_like(y)
   for i in range(nr+1):
      # build an incident ray, parallel to optical axis
      # at a distance less than 10/2=5% of first surface diameter
      y[i] = (i-nr//2)*0.1*syst[0].diameter/nr
      r = optics.Ray(
            input_oaxis.origin + y[i]*vortho,
            input_oaxis.direction,
            1.0,
            wavelength)
      # propagate through the system
      output_ray = propagate(syst, [r])[-1][0]
      # get the output ray, and measure its angle with respect to optical axis
      cosi = np.dot(output_ray.direction, output_oaxis.direction)
      sini = np.dot(output_ray.direction,
                    geometry.orthogonal(output_oaxis.direction))
      angle[i] = np.arctan2(sini, cosi)
   # fit a straight line, return the coefficient & datas
   pol = np.polyfit(angle, y, 1)
   return pol[0], np.vstack((y, angle))

def bfl(syst: List[optics.Surface], wavelength: float) -> np.ndarray:
   """
   Compute the back focal length (B.F.L.) of an optical system
   for a given wave length and rays at differents distances
   from the optical axis.
   It returns an array wih two lines.
   - First line is the BFL of a ray
   - Second line is the distance of the ray from the optical axis
   """
   # get the input & output optical axis
   oaxis = optical_axis(syst)
   input_oaxis = oaxis[0]
   output_oaxis = oaxis[-2]
   # local frame
   C = output_oaxis.origin
   B = np.vstack(( output_oaxis.direction,
                   geometry.orthogonal(output_oaxis.direction))).T
   # get a direction orthogonal to the input optical axis
   vortho = geometry.orthogonal(input_oaxis.direction)
   # nb of test rays
   nr = 32
   # prepare the output arrays:
   # - y is the distance to the input optical axis
   # - intersect is the distance along the optical axis to the
   #   intersect of the ray
   y = np.zeros((nr+1,), dtype=np.float64)
   intersect = np.zeros_like(y)
   for i in range(1, nr+1):
      # build an incident ray, parallel to optical axis
      # at a distance less than 45% of first surface semi-diameter
      y[i] = i*0.45*syst[0].diameter/nr
      r = optics.Ray(
            input_oaxis.origin + y[i]*vortho,
            input_oaxis.direction,
            1.0,
            wavelength)
      # propagate through the system
      output_ray = propagate(syst, [r])[-2][0]
      # to local frame
      xprime = np.dot(B.T, output_ray.origin -C)
      uprime = np.dot(B.T, output_ray.direction)
      # get the output ray, and its intersection with the
      # output optical axis
      pt = geometry.intersect_ray_line(xprime, uprime)
      if pt.size == 0:
         intersect[i] = 0.
      else:
         # to global frame
         pt = C + np.dot(B, pt)
         intersect[i] = norm(pt - output_oaxis.origin)
   # remove the 0 entries, no intersect
   inz = intersect != 0
   return np.vstack((intersect[inz], y[inz]))
