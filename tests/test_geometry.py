"""
Unit tests of geometry.py
"""
import pytest
import numpy as np
from numpy.linalg import norm
import geometry

@pytest.fixture
def theta():
   return 35*np.pi/180

def test_ilc1():
   R = 1.0
   X = np.zeros((2,), dtype=np.float64)
   U = np.array((1., 1.), dtype=np.float64)
   U /= norm(U)
   t = geometry.intersect_line_circle(R, X, U)
   np.testing.assert_allclose(t, np.array((-1., 1.), dtype=np.float64))

def test_ilc2():
   R = 1.0
   X = np.array((-2., 0.), dtype=np.float64)
   U = np.array((1., 0.), dtype=np.float64)
   t = geometry.intersect_line_circle(R, X, U)
   np.testing.assert_allclose(t, np.array((1., 3.), dtype=np.float64))

def test_ilc3():
   R = 1.0
   X = np.array((-2., 2.), dtype=np.float64)
   U = np.array((1., 0.), dtype=np.float64)
   t = geometry.intersect_line_circle(R, X, U)
   assert t.size == 0

def test_ilc4(theta):
   R = 1.0
   U = np.array((np.cos(theta), np.sin(theta)), dtype=np.float64)
   X = 10*U
   U = -U
   t = geometry.intersect_line_circle(R, X, U)
   np.testing.assert_allclose(t, np.array((9., 11.), dtype=np.float64))

def test_irc1():
   R = 1.0
   X = np.zeros((2,), dtype=np.float64)
   U = np.array((1., 1.), dtype=np.float64)
   U /= norm(U)
   t = geometry.intersect_ray_circle(R, X, U)
   np.testing.assert_allclose(t, np.array((1.,), dtype=np.float64))

def test_irc2():
   R = 1.0
   X = np.array((-2., 0.), dtype=np.float64)
   U = np.array((1., 0.), dtype=np.float64)
   t = geometry.intersect_ray_circle(R, X, U)
   np.testing.assert_allclose(t, np.array((1.,), dtype=np.float64))

def test_irc3():
   R = 1.0
   X = np.array((-2., 0.), dtype=np.float64)
   U = np.array((-1., 0.), dtype=np.float64)
   t = geometry.intersect_ray_circle(R, X, U)
   assert t.size == 0

def test_refraction1():
   r = np.array((1, -1), dtype=np.float64)
   r /= norm(r)
   v = np.array((0, 1), dtype=np.float64)
   np.testing.assert_allclose(geometry.refraction(r, v, 1., 1.), r)

def test_refraction2():
   # incident ray
   r = np.array((1, -1), dtype=np.float64)
   r /= norm(r)
   # normal
   v = np.array((0, 1), dtype=np.float64)
   # reflected ray
   rr = np.array((1., 1.), dtype=np.float64)
   rr /= norm(rr)
   np.testing.assert_allclose(geometry.refraction(r, v, 1.5, 1.0), rr)

def test_refraction3(theta):
   # incident ray
   theta1 = theta
   r1 = np.array((np.sin(theta1), -np.cos(theta1)), dtype=np.float64)
   n1 = 1.0
   # normal
   v = np.array((0, 1), dtype=np.float64)
   # refracted ray
   n2 = 1.5
   theta2 = np.arcsin(n1*np.sin(theta1)/n2)
   r2 = np.array((np.sin(theta2), -np.cos(theta2)), dtype=np.float64)
   np.testing.assert_allclose(geometry.refraction(r1, v, n1, n2), r2)

def test_reflect1():
   # incident ray
   r = np.array((1., -1.), dtype=np.float64)
   r /= norm(r)
   # normal
   v = np.array((0, 1), dtype=np.float64)
   # reflected ray
   rr = np.array((1., 1.), dtype=np.float64)
   rr /= norm(rr)
   np.testing.assert_allclose(geometry.reflection(r, v), rr)

def test_reflect2():
   r = np.array((0., -1.), dtype=np.float64)
   v = np.array((0, 1), dtype=np.float64)
   np.testing.assert_allclose(geometry.reflection(r, v),
                              np.array((0., 1.), dtype=np.float64))

def test_reflect3(theta):
   r1 = np.array((np.sin(theta), -np.cos(theta)), dtype=np.float64)
   v = np.array((0, 1), dtype=np.float64)
   r2 = np.array((np.sin(theta), np.cos(theta)), dtype=np.float64)
   np.testing.assert_allclose(geometry.reflection(r1, v), r2)

def test_ira1():
   x = np.zeros((2,), dtype=np.float64)
   u = np.array((1, 0), dtype=np.float64)
   p = geometry.intersect_ray_arc(1.0, x, u, 0.5)
   np.testing.assert_allclose(p, u)

def test_ira2():
   x = np.array((0, 0.6), dtype=np.float64)
   u = np.array((1, 0), dtype=np.float64)
   p = geometry.intersect_ray_arc(1.0, x, u, 0.5)
   assert p.size == 0

def test_ira3():
   yi = 0.15
   xi = np.sqrt(1.0 - yi**2)
   x = np.array((-2., yi), dtype=np.float64)
   u = np.array((1, 0), dtype=np.float64)
   p = geometry.intersect_ray_arc(1.0, x, u, 0.5)
   np.testing.assert_allclose(p, np.array((xi, yi), dtype=np.float64))

def test_ira4():
   x = np.array((-2, 2), dtype=np.float64)
   u = np.array((1, 0), dtype=np.float64)
   p = geometry.intersect_ray_arc(1.0, x, u, 0.5)
   assert p.size == 0

def test_ira5(theta):
   u0 = np.array((np.cos(theta), np.sin(theta)), dtype=np.float64)
   x = 10*u0
   u = -u0
   p = geometry.intersect_ray_arc(1.0, x, u, 1.3)
   np.testing.assert_allclose(p, u0)

def test_ira6():
   x = np.zeros((2,), dtype=np.float64)
   u = np.array((-1, 0), dtype=np.float64)
   p = geometry.intersect_ray_arc(1.0, x, u, 0.5, east=False)
   np.testing.assert_allclose(p, u)

def test_ira7(theta):
   u0 = np.array((np.cos(theta+np.pi), np.sin(theta+np.pi)), dtype=np.float64)
   x = 10*u0
   u = -u0
   p = geometry.intersect_ray_arc(1.0, x, u, 1.3, east=False)
   np.testing.assert_allclose(p, u0)

def test_irl1():
   x = np.array((-6, 4), dtype=np.float64)
   u = np.array((1, -1), dtype=np.float64)
   u /= norm(u)
   pt = geometry.intersect_ray_line(x, u)
   np.testing.assert_allclose(pt, np.array((-2, 0), dtype=np.float64))

def test_irl2():
   x = np.array((-6, 4), dtype=np.float64)
   u = np.array((1, 1), dtype=np.float64)
   u /= norm(u)
   assert geometry.intersect_ray_line(x, u).size == 0

def test_irs1():
   x = np.array((-6, 4), dtype=np.float64)
   u = np.array((1, -1), dtype=np.float64)
   u /= norm(u)
   pt = geometry.intersect_ray_segment(x, u, 5)
   np.testing.assert_allclose(pt, np.array((-2, 0), dtype=np.float64))

def test_irs2():
   x = np.array((-6, 4), dtype=np.float64)
   u = np.array((1, -1), dtype=np.float64)
   u /= norm(u)
   assert geometry.intersect_ray_segment(x, u, 3).size == 0

def test_ilp1():
   x = np.array((-1, 1), dtype=np.float64)
   u = -x/ norm(x)
   t = geometry.intersect_line_parabola(x, u, 1.0)
   # llok for the greatest positive root
   np.testing.assert_allclose(np.max(t), np.sqrt(2))

def common_parabola_test_datas():
   # set focal length
   f = 2.22
   # pick a point along the parabola
   x = 1.5
   z = x**2 / (4*f)
   # create an arbitray displacement vector
   displace = np.array((-1, 3), dtype=np.float64)
   # starting point of line
   X = np.array((x, z), dtype=np.float64) + displace
   # direction of line
   U = - displace / norm(displace)
   return X, U, displace, f

def test_ilp2():
   X, U, displace, f = common_parabola_test_datas()
   # intersect
   t = geometry.intersect_line_parabola(X, U, f)
   # look for the greatest positive root
   np.testing.assert_allclose(np.max(t), norm(displace))

def test_irp1():
   X, U, displace, f = common_parabola_test_datas()
   # intersect
   pt = geometry.intersect_ray_parabola(X, U, f, 3.5)
   # look for the greatest positive root
   np.testing.assert_allclose(pt, X + norm(displace) * U)

def test_irp2():
   X, U, _, f = common_parabola_test_datas()
   # intersect
   t = geometry.intersect_ray_parabola(X, U, f, 2)
   # outside of arc of parabola
   assert t.size == 0

def test_refl_grat1():
   # zero incidence
   sinbeta = n_m_lambda = 1 * 600 * 0.0005
   cosbeta = np.sqrt(1 - sinbeta**2)
   ri = np.array((0, -1), dtype=np.float64)
   rr = geometry.diffraction_grating(ri, n_m_lambda, True)
   np.testing.assert_allclose(
         rr, np.array((-sinbeta, cosbeta), dtype=np.float64))

def test_refl_grat2():
   # Littrow condition
   n_m_lambda = 1 * 600 * 0.0005
   sina = 0.5 * n_m_lambda
   cosa = np.sqrt(1 - sina**2)
   ri = np.array((sina, -cosa), dtype=np.float64)
   rr = geometry.diffraction_grating(ri, n_m_lambda, True)
   np.testing.assert_allclose(rr, -ri)

def test_refl_grat3():
   # not incident
   n_m_lambda = 1 * 600 * 0.0005
   ri = np.array((-1, 1), dtype=np.float64)
   ri /= norm(ri)
   assert geometry.diffraction_grating(ri, n_m_lambda, True).size == 0

def test_refl_grat4():
   # no diffraction at zero incidence with high density grating
   n_m_lambda = 1 * 2400 * 0.0005
   ri = np.array((0, -1), dtype=np.float64)
   assert geometry.diffraction_grating(ri, n_m_lambda, True).size == 0

def test_trans_grat1():
   # zero incidence
   sinbeta = n_m_lambda = 1 * 600 * 0.0005
   cosbeta = np.sqrt(1 - sinbeta**2)
   ri = np.array((0, -1), dtype=np.float64)
   rr = geometry.diffraction_grating(ri, n_m_lambda, False)
   np.testing.assert_allclose(
         rr, np.array((sinbeta, -cosbeta), dtype=np.float64))

def test_trans_grat2(theta):
   # direct transmission, no diffraction
   ri = np.array((np.sin(theta), -np.cos(theta)), dtype=np.float64)
   rr = geometry.diffraction_grating(ri, 0, False)
   np.testing.assert_allclose(rr, ri)
