"""
A singulet lens, and a star diagonal
"""
import sys
import numpy as np
import medium
import optics
import analysis
import lens

# optical media
glass = medium.ConstantMedium(1.5)

# design a biconvex lens
radius = 500.0
diam = 150.0
half_width = lens.half_width(radius, diam)

# a biconvex lens followed by a star diagonal 
# and terminated by a screen at approximate focus
sys2 = [
      optics.Sphere(
         np.array((0., 0.)),
         np.array((-1., 0.)),
         radius,
         diam,
         glass),
      optics.Sphere(
         np.array((2*half_width, 0.)),
         np.array((-1., 0.)),
         -radius,
         diam,
         medium.air),
      optics.PlaneMirror(
         np.array((450, 0)),
         np.array((-1., 1.)),
         50),
      optics.Screen(
         np.array((450, 100.0)),
         np.array((0., -1.)),
         50)
      ]

n = 5  # number of rays
theta = 0.5 * np.pi/180  # inclination angle
# create the rays
rb = analysis.make_ray_bundle(-50, -50, 50, n, theta, 500)
# propagate through the system
rbs = analysis.propagate(sys2, rb)
# get the optical axis
oax = analysis.optical_axis(sys2)
# draw it into an asymptote file
filename = sys.argv[0]
analysis.draw(filename.replace(".py", ".asy"), sys2, rbs, oax)
# effective focal length analysis
import matplotlib.pyplot as plt
efl, y_a = analysis.efl(sys2, 500)
plt.grid(True)
plt.plot(y_a[0,:]*180.0/np.pi, y_a[1,:], "k")
plt.xlabel("angle [deg]")
plt.ylabel("y")
plt.title("EFL = {:12.5e}".format(efl))
plt.show()

