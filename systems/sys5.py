"""
A classical spectrograph
"""
import sys
import numpy as np
import medium
import optics
import analysis
import lens

# optical media
glass = medium.ConstantMedium(1.5)

# design a biconvex lens: telescope objective
radius1 = 500.0
diam1 = 80.0
half_width1 = lens.half_width(radius1, diam1)
# design a biconvex lens: collimating and camera lenses
radius2 = 125.0
diam2 = 50.0
half_width2 = lens.half_width(radius2, diam2)
# position of grating
pgrating = np.array((radius1+radius2+140, 0))
# diffraction angle and direction of reflected green ray
adiffrac = 18*np.pi/180.0
vdiffrac = -np.array((np.cos(adiffrac), np.sin(adiffrac)))
# distance grating to camera lens
dgl = 120

sys1 = [
      # telescope objective
      optics.Sphere(
         np.array((0., 0.)),
         np.array((-1., 0.)),
         radius1,
         diam1,
         glass),
      optics.Sphere(
         np.array((2*half_width1, 0.)),
         np.array((-1., 0.)),
         -radius1,
         diam1,
         medium.air),
      # slit
      optics.TransparentPlane(
         np.array((radius1-half_width1, 0)),
         np.array((-1., 0.)),
         30,
         medium.air),
      # collimating lens
      optics.Sphere(
         np.array((radius1+radius2, 0)),
         np.array((-1., 0.)),
         radius2,
         diam2,
         glass),
      optics.Sphere(
         np.array((radius1+radius2+2*half_width2, 0)),
         np.array((-1., 0.)),
         -radius2,
         diam2,
         medium.air),
      # reflection grating
      optics.DiffractionGrating(
         pgrating,
         np.array((-1., 0.)),
         50,
         600,
         True),
      # camera lens
      optics.Sphere(
         pgrating + dgl*vdiffrac,
         -vdiffrac,
         radius2,
         diam2,
         glass),
      optics.Sphere(
         pgrating + (dgl+2*half_width2)*vdiffrac,
         -vdiffrac,
         -radius2,
         diam2,
         medium.air),
      # sensor
      optics.Screen(
         pgrating + (dgl+half_width2+radius2)*vdiffrac,
         -vdiffrac,
         32)
      ]

n = 5  # number of rays
theta = 0  # inclination angle
# create the rays
rb400 = analysis.make_ray_bundle(-50, -33, 37, n, theta, 400)
rb500 = analysis.make_ray_bundle(-50, -35, 35, n, theta, 500)
rb650 = analysis.make_ray_bundle(-50, -37, 33, n, theta, 650)
# propagate through the system
rbs = analysis.propagate(sys1, rb400 + rb500 + rb650)
# get the optical axis
oax = analysis.optical_axis(sys1)
# draw it into an asymptote file
filename = sys.argv[0]
analysis.draw(filename.replace(".py", ".asy"), sys1, rbs, oax)
