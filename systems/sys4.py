"""
Divergent thin lens
"""
import sys
import numpy as np
import medium
import optics
import analysis
import lens

# optical media
glass = medium.ConstantMedium(1.5)

# design a divergent lens
radius = 500.0
diam = 150.0
half_width = lens.half_width(radius, diam)

# a biconcave lens
sys1 = [
      optics.Sphere(
         np.array((0., 0.)),
         np.array((-1., 0.)),
         -radius,
         diam,
         glass),
      optics.Sphere(
         np.array((half_width, 0.)),
         np.array((-1., 0.)),
         radius,
         diam,
         medium.air),
      optics.FreeSpace(150)
      ]

n = 6  # number of rays
theta = 0.0  # inclination angle
# create the rays
rb = analysis.make_ray_bundle(-50, -50, 50, n, theta, 500)
# propagate through the system
rbs = analysis.propagate(sys1, rb)
# get the optical axis
oax = analysis.optical_axis(sys1)
# draw it into an asymptote file
filename = sys.argv[0]
analysis.draw(filename.replace(".py", ".asy"), sys1, rbs, oax)
