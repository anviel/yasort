"""
Achromatic doublet
"""
import sys
import numpy as np
import geometry
import medium
import optics
import analysis
import lens

# media
crown = medium.FK5
flint = medium.LaF2
wl1 = 486 # first corrected wavelength
wl2 = 656 # second corrected wavelength
wlc = 500 # central wavelength => EFL

# design an achromatic doublet: biconvex + planoconcave
# target focal length is 400 mm, diameter 80 mm => f/5
# A lot of spherical aberration is expected!
diam = 80.0
radius1, radius2 = lens.simple_achromat(400, crown, flint, wl1, wl2, 500, diam)
half_width1 = lens.half_width(radius1, diam)
half_width2 = lens.half_width(radius2, diam)
width2 = half_width2

# common normal of all centered surfaces
Xm = np.array((-1., 0.))

# a biconvex lens in crown followed by a planoconcave lens in flint
sys1 = [
      optics.Sphere(
         np.array((0., 0.)),
         Xm,
         radius1,
         diam,
         crown),
      optics.Sphere(
         np.array((half_width1+half_width2, 0.)),
         Xm,
         radius2,
         diam,
         flint),
      optics.TransparentPlane(
         np.array((half_width1+half_width2+width2, 0.)),
         Xm,
         diam,
         medium.air),
      optics.Screen(
         np.array((378+half_width1+half_width2+width2, 0.0)),
         Xm,
         diam)
      ]

# A simple ray bundle analysis
n = 7  # number of rays
#theta = 1.5 * np.pi/180  # inclination angle
theta = 0
# create the rays
rb = analysis.make_ray_bundle(-50, -0.5*diam+1, 0.5*diam-1, n, theta, wlc)
# propagate through the system
rbs = analysis.propagate(sys1, rb)
# get the optical axis
oax = analysis.optical_axis(sys1)
# draw it into an asymptote file
filename = sys.argv[0]
analysis.draw(filename.replace(".py", ".asy"), sys1, rbs, oax)

# Effective focal length analysis
import matplotlib.pyplot as plt
efl, y_a = analysis.efl(sys1, wlc)
plt.figure(10)
plt.grid(True)
plt.plot(y_a[1,:]*180.0/np.pi, y_a[0,:], "k")
plt.xlabel("angle [deg]")
plt.ylabel("y")
plt.title("EFL = {:12.5e}".format(efl))

# Back focal length analysis
bfl = []
wavelen_range = (wl1, wlc, wl2)
for wl in wavelen_range:
   b_y = analysis.bfl(sys1, wl)
   bfl.append(b_y[0, :])
   y   = b_y[1, :]
mean_bfl = np.mean(np.hstack(bfl))
plt.figure(11)
plt.grid(True)
plt.plot(bfl[0]-mean_bfl, y, optics.ray_color(wavelen_range[0])[0],
         bfl[1]-mean_bfl, y, optics.ray_color(wavelen_range[1])[0],
         bfl[2]-mean_bfl, y, optics.ray_color(wavelen_range[2])[0])
plt.xlabel("distance along optical axis")
plt.ylabel("distance above optical axis")
plt.title("mean BFL (@ center) = {:12.5e}".format(mean_bfl))
plt.legend(["$\lambda$ = {} nm".format(wl) for wl in wavelen_range])
plt.show()
