"""
A paraboloid mirror
"""
import sys
import numpy as np
import medium
import optics
import analysis
import lens

# optical media

# design a paraboloid mirror
f = 500.0
diam = 150.0

sys3 = [
      optics.ParaboloidMirror(
         np.array((f, 0.)),
         np.array((-1., 0.)),
         -f,
         diam),
      optics.Screen(
         np.array((1., 0.)),
         np.array((1., 0.)),
         15)
      ]

n = 5  # number of rays
theta = 0.5 * np.pi/180  # inclination angle
# create the rays
rb = analysis.make_ray_bundle(-75, -70, 70, n, theta, 500)
# propagate through the system
rbs = analysis.propagate(sys3, rb)
# get the optical axis
oax = analysis.optical_axis(sys3)
# draw it into an asymptote file
filename = sys.argv[0]
analysis.draw(filename.replace(".py", ".asy"), sys3, rbs, oax)
# effective focal length analysis
import matplotlib.pyplot as plt
efl, y_a = analysis.efl(sys3, 500)
plt.grid(True)
plt.plot(y_a[0,:]*180.0/np.pi, y_a[1,:], "k")
plt.xlabel("angle [deg]")
plt.ylabel("y")
plt.title("EFL = {:12.5e}".format(efl))
plt.show()


